import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hackaton/main.dart';
import 'package:hackaton/ui/pages/home-page.dart';
import 'package:hackaton/ui/components/top-bar.dart';
import 'package:camera/camera.dart';
import 'package:hackaton/ui/pages/settings-page.dart';
import 'package:device_info_plus/device_info_plus.dart';

class App extends StatefulWidget {
  const App({super.key, required this.camera});
  final CameraDescription camera;

  @override
  AppState createState() => AppState();
}

class AppState extends State<App> {
  @override
  void initState() {
    super.initState();
    getDeviceID();
  }
  // void setupPermissions(){
  //     Permission.accessNotificationPolicy
  // }

  Future<void> getDeviceID() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      logger.i("INFO $androidInfo");
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      logger.i("INFO $iosInfo");
    }
  }

  @override
  Widget build(BuildContext context) {
    // initialRoute: "/",

    return MaterialApp(
      title: 'Flutter Demo dz',
      theme: ThemeData(
        useMaterial3: true,
        // colorScheme: color_theme,
        scaffoldBackgroundColor: Colors.black54,
        brightness: Brightness.dark,
      ),
      initialRoute: "/",
      routes: {
        '/settings': (context) => const SettingsPage(),
      },
      builder: EasyLoading.init(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: const PreferredSize(
          preferredSize: Size.fromHeight(55.0),
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: TopBar(),
          ),
        ),
        body: HomePage(camera: widget.camera),
      ),
    );
    // var color_theme = ColorScheme.fromSwatch(
    //   brightness: Brightness.dark,
    //   primarySwatch: env.config_accent_color,
    // );
  }
}
