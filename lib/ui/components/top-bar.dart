import 'package:flutter/material.dart';
import 'package:hackaton/app_config.dart' as env;
import 'package:hackaton/main.dart';

class TopBar extends StatelessWidget {
  const TopBar({super.key, this.showLogo = true});
  final bool showLogo;
  final Widget title = const Text(
    "ImageChat",
    style: TextStyle(fontSize: 18),
  );
  final backgroundColor = Colors.black54;
  final foregroundColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: showLogo
          ? ClipRect(
              child: Image.asset(env.logo_path),
            )
          : null,
      title: title,
      actions: <Widget>[
        Ink(
            decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: env.config_secondary_background_color),
            child: IconButton(
              icon: const Icon(Icons.settings, color: Colors.white),
              // backgroundColor: Colors.white,
              onPressed: () => (onSettingsButtonPressed(context)),
            ))
      ],
      backgroundColor: backgroundColor,
      foregroundColor: foregroundColor,
    );
  }

  void onSettingsButtonPressed(BuildContext context) {
    logger.i("settings pressed");
    Navigator.pushNamed(context, '/settings');
  }
}
