import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'dart:io';
import 'dart:convert';
import 'package:hackaton/app_config.dart' as env;
import 'package:hackaton/main.dart';
import 'package:hackaton/services/server-requests.dart';
import 'package:hackaton/core/jsonHelper.dart';

class PromptsTable extends StatelessWidget {
  PromptsTable({super.key, required this.imagePath});
  final String imagePath;

  List<String> prompt_cache = [];
  List<String> agent_response_cache = [];
  @override
  Widget build(BuildContext context) {
    // this.image = File(imagePath);
    return FutureBuilder(
      future: promptsFile,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          // print(jsonDecode(snapshot.data!.readAsStringSync()));
          var json = jsonDecode(snapshot.data!.readAsStringSync());
          var prompts = json['prompts'];
          var maxLen = json['maxTokenLength'];
          var langIn = json["inputLanguage"];
          var langOut = json["outputLanguage"];
          var promptLen = prompts.length;
          logger.v("Input lang: $langIn\nOutput lang: $langOut");
          List<Widget> gridChildren = List.generate(promptLen, (int index) {
            return Column(
              children: [
                SizedBox(
                  height: 80,
                  width: MediaQuery.of(context).size.width,
                  child: ElevatedButton(
                    onPressed: () async {
                      if (locked) {
                        EasyLoading.showError(
                            "Other prompt is being processed. Please wait.");
                        return;
                      }
                      File imageFile = File(imagePath);
                      prompt_cache.add(prompts[index]);
                      var response = await sendPictureAndPrompt(
                          prompts[index],
                          imageFile,
                          langIn,
                          langOut,
                          maxLen,
                          prompt_cache,
                          agent_response_cache);
                      agent_response_cache.add(response);
                    },
                    style: FilledButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    child: Text(
                      style: const TextStyle(
                          color: env.config_foreground_color, fontSize: 20),
                      "${prompts[index]}",
                    ),
                  ),
                ),
              ],
            );
          });
          return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.builder(
                itemCount: gridChildren.length,
                itemBuilder: (context, index) {
                  final tileModel = gridChildren[index];
                  return Container(
                    decoration: BoxDecoration(border: Border.all()),
                    padding: const EdgeInsets.all(8),
                    margin: const EdgeInsets.symmetric(vertical: 1),
                    height: 100,
                    alignment: Alignment.centerLeft,
                    child: tileModel,
                  );
                },
              ));
        }
        return const CircularProgressIndicator();
      },
    );
  }
}
